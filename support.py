from rox import g, saving
import rox
import fcntl
from rox import processes
import sys, os

def shell_escape(text):
	"""Return text with \ and ' escaped"""
	return text.replace("\\", "\\\\").replace("'", "\\'")

def Tmp(mode = 'w+b'):
	"Create a seekable, randomly named temp file (deleted automatically after use)."
	import tempfile
	try:
		return tempfile.NamedTemporaryFile(mode, suffix = '-archive')
	except:
		# python2.2 doesn't have NamedTemporaryFile...
		pass

	import random
	name = tempfile.mktemp(`random.randint(1, 1000000)` + '-archive')

	fd = os.open(name, os.O_RDWR|os.O_CREAT|os.O_EXCL, 0700)
	tmp = tempfile.TemporaryFileWrapper(os.fdopen(fd, mode), name)
	tmp.name = name
	return tmp

def keep_on_exec(fd):
	fcntl.fcntl(fd, fcntl.F_SETFD, 0)
